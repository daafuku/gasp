package gasp

import (
	"encoding/base32"
	"math/rand"
	"net/http"
)

func generateRequestId() string {
	var b []byte = make([]byte, 10)
	rand.Read(b)
	return base32.StdEncoding.EncodeToString(b)
}

func RequestIdMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		ctx := getRequestContext(req)
		ctx.requestID = generateRequestId()
		next.ServeHTTP(res, req)
	})
}
