package gasp

import (
	"fmt"
	"log"
	"net/http"
	"runtime"
)

func ErrorHandlerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		defer func() {
			if err := recover(); err != nil && err != http.ErrAbortHandler {
				const size = 64 << 10
				buf := make([]byte, size)
				buf = buf[:runtime.Stack(buf, false)]

				err := fmt.Errorf("http: panic serving: %v\n%s", err, buf)

				res.WriteHeader(http.StatusInternalServerError)

				fmt.Fprintf(res, "%s", err)
				log.Println(err)
				return
			}

		}()

		next.ServeHTTP(res, req)

		for _, err := range getRequestContext(req).errs {
			log.Println(err)
		}
	})
}

func addError(req *http.Request, err error) {
	errs := getRequestContext(req).errs
	errs = append(errs, err)
}
