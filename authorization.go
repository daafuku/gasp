package gasp

import (
	"net/http"

	"github.com/gorilla/mux"
)

func HasIdentityMiddleware() mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			ctx := getRequestContext(req)
			if ctx.identity != nil {
				next.ServeHTTP(res, req)
				return
			}

			res.WriteHeader(http.StatusUnauthorized)
		})
	}
}

func HasClaimMiddleware(key, value string) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			if hasClaim(req, key, value) {
				next.ServeHTTP(res, req)
				return
			}

			res.WriteHeader(http.StatusForbidden)
		})
	}
}

func hasClaim(req *http.Request, key, value string) bool {
	if req == nil {
		return false
	}

	id := getRequestContext(req).identity
	if id == nil {
		return false
	}

	if claims, ok := id.Claims[key]; !ok {
		return false
	} else {
		for _, claim := range claims {
			if claim == value {
				return true
			}
		}
	}
	return false
}
