package gasp

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
)

type RequestContextKey int

const (
	ContextRequestContextKey = RequestContextKey(iota)
)

type Context struct {
	requestID string
	identity  *Identity
	errs      []error
}

func getRequestContext(req *http.Request) *Context {
	ctx, ok := req.Context().Value(ContextRequestContextKey).(*Context)
	if !ok || ctx == nil {
		panic("context is not present or nil")
	}
	return ctx
}

func GASPContextMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		req = req.WithContext(
			context.WithValue(
				req.Context(),
				ContextRequestContextKey,
				new(Context),
			))

		next.ServeHTTP(res, req)
	})
}

func SerialiseIdentity(identity *Identity) ([]byte, error) {
	buf := &bytes.Buffer{}
	err := json.NewEncoder(buf).Encode(identity)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func DeserialiseIdentity(b []byte) (identity *Identity, err error) {
	err = json.NewDecoder(bytes.NewReader(b)).Decode(&identity)
	return
}
