package gasp

import (
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
)

type LoginIdentityProvider struct {
	Base UsernamePasswordIdentityProvider
}

func (idp *LoginIdentityProvider) Identity(req *http.Request) (*Identity, error) {
	err := req.ParseForm()
	if err != nil {
		return nil, err
	}

	username := req.Form.Get("username")
	password := req.Form.Get("password")

	return idp.Base.Identity(username, password)
}

func FormLogin(r *mux.Router, path string,
	idp UsernamePasswordIdentityProvider,
	ids IdentityStorage) {

	login := r.MatcherFunc(func(req *http.Request, rm *mux.RouteMatch) bool {
		return req.Method == http.MethodPost && req.URL.Path == path
	}).Subrouter()

	login.Use(
		IdentityProviderMiddleware(&LoginIdentityProvider{Base: idp}),
		PersistIdentityMiddleware(ids))

	login.HandleFunc(path, func(res http.ResponseWriter, req *http.Request) {
		uri, _ := url.Parse(req.FormValue("redirect_uri"))
		location := "/"
		if uri != nil && uri.Path != "" {
			location = uri.Path
		}

		res.Header().Add("Location", location)
		res.WriteHeader(http.StatusFound)
	})
}
