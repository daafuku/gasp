package gasp

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type UsernamePasswordIdentityProvider interface {
	Identity(username, password string) (*Identity, error)
}

type StaticUsernamePasswordIdentityProvider struct {
	Username string
	Password string
}

func (provider *StaticUsernamePasswordIdentityProvider) Identity(username, password string) (id *Identity, err error) {
	if username == provider.Username && password == provider.Password {
		id = &Identity{
			Issuer: "static",
			Claims: map[string][]string{
				"username": {username},
			},
		}
		return
	}

	err = fmt.Errorf("invalid username and password combination")
	return
}

func IdentityProviderMiddleware(idp IdentityProvider) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			if identity, err := idp.Identity(req); err != nil {
				res.WriteHeader(http.StatusUnauthorized)
				res.Write(nil)
				log.Println(err)
				return
			} else if identity != nil {
				getRequestContext(req).identity = identity
			}

			next.ServeHTTP(res, req)
		})
	}
}
