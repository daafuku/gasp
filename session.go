package gasp

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
)

type SessionIdentityProvider struct {
	Name   string
	Issuer string
	Store  sessions.Store
}

const issuerName = "session"

func (idp *SessionIdentityProvider) Identity(req *http.Request) (*Identity, error) {
	session, err := idp.Store.Get(req, idp.Name)

	if err != nil {
		return nil, err
	}

	if serialisedIdentity, ok := session.Values["identity"].([]byte); ok {
		identity, err := DeserialiseIdentity(serialisedIdentity)
		return identity, err
	}

	return nil, nil
}

type SessionIdentityStorage struct {
	Name  string
	Store sessions.Store
}

func (is *SessionIdentityStorage) PersistIdentity(res http.ResponseWriter, req *http.Request, identity *Identity) error {
	session, err := is.Store.New(req, is.Name)

	if err != nil {
		return err
	}

	serialisedIdentity, err := SerialiseIdentity(identity)
	if err != nil {
		return err
	}

	session.Values["identity"] = serialisedIdentity

	err = session.Save(req, res)
	if err != nil {
		return err
	}

	return nil
}

func PersistIdentityMiddleware(ids IdentityStorage) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
			err := ids.PersistIdentity(res, req, getRequestContext(req).identity)
			if err != nil {
				log.Println(err)
			}
			next.ServeHTTP(res, req)
		})
	}
}
