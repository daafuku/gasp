package gasp

import "net/http"

type Identity struct {
	Issuer string
	Claims map[string][]string
}

type IdentityProvider interface {
	Identity(*http.Request) (*Identity, error)
}

type IdentityStorage interface {
	PersistIdentity(http.ResponseWriter, *http.Request, *Identity) error
}
