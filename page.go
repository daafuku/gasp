package gasp

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/gorilla/mux"
)

type PageData struct {
	Identity *Identity
}

func PageHandler(res http.ResponseWriter, req *http.Request) {

	funcMap := template.FuncMap{
		"identity": func() *Identity { return getRequestContext(req).identity },
	}

	templates, err := template.
		New("root").
		Funcs(funcMap).
		ParseFiles("templates/layout.html")
	if err != nil {
		addError(req, err)
		panic(err)
	}

	page, ok := mux.Vars(req)["page"]

	if !ok || page == "" {
		page = "index"
	}

	log.Println("page", page)

	sourceFile := strings.Join([]string{page, "html"}, ".")

	templates, err = templates.ParseFiles(
		filepath.Join(
			"templates",
			sourceFile,
		))

	if err != nil {
		addError(req, err)
		if _, is := err.(*os.PathError); is {
			res.WriteHeader(http.StatusNotFound)
			res.Write([]byte(fmt.Sprintf("page %s not found", page)))
		} else {
			panic(err)
		}
		return
	}

	buf := &bytes.Buffer{}

	err = templates.ExecuteTemplate(buf, "basic", nil)
	if err != nil {
		panic(err)
	}

	res.Header().Set("Content-Type", "text/html; charset=UTF-8")
	res.WriteHeader(http.StatusOK)
	_, err = io.Copy(res, buf)
	if err != nil {
		panic(err)
	}

}
