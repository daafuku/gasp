package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.com/daafuku/gasp"
)

func main() {
	r := mux.NewRouter()

	r.Use(gasp.GASPContextMiddleware)
	r.Use(gasp.RequestIdMiddleware)
	r.Use(gasp.LoggingMiddleware)
	r.Use(gasp.ErrorHandlerMiddleware)

	sessionStore := sessions.NewCookieStore([]byte("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"))

	r.Use(gasp.IdentityProviderMiddleware(&gasp.SessionIdentityProvider{
		Name:  "gasp",
		Store: sessionStore,
	}))

	gasp.FormLogin(r, "/login",
		&gasp.StaticUsernamePasswordIdentityProvider{
			Username: "tom",
			Password: "password",
		},
		&gasp.SessionIdentityStorage{
			Name:  "gasp",
			Store: sessionStore,
		})

	r.HandleFunc("/", gasp.PageHandler)
	r.HandleFunc("/{page}", gasp.PageHandler)

	server := http.Server{
		Addr:    ":8000",
		Handler: r,
	}

	err := server.ListenAndServe()
	log.Println(err)
}
