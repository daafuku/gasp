package gasp

import (
	"log"
	"net/http"
	"time"
)

type loggingResponseWriter struct {
	started     time.Time
	wroteHeader time.Time
	lastWrite   time.Time
	statusCode  int
	path        string

	base http.ResponseWriter
}

func (lrw *loggingResponseWriter) Header() http.Header {
	return lrw.base.Header()
}

func (lrw *loggingResponseWriter) Write(p []byte) (int, error) {
	defer func() {
		lrw.lastWrite = time.Now()
	}()

	return lrw.base.Write(p)
}

func (lrw *loggingResponseWriter) WriteHeader(statusCode int) {
	lrw.base.WriteHeader(statusCode)
	lrw.statusCode = statusCode
	lrw.wroteHeader = time.Now()
}

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res = &loggingResponseWriter{
			base:    res,
			started: time.Now(),
			path:    req.URL.Path,
		}

		next.ServeHTTP(res, req)

		var username string

		ctx := getRequestContext(req)
		identity := ctx.identity

		if identity != nil {
			if usernames, ok := identity.Claims["username"]; ok && len(usernames) > 0 {
				username = usernames[0]
			}
		}

		if username == "" {
			username = "nobody"
		}

		log.Printf("[%s] %s@%s %s %d %s %s \n",
			ctx.requestID,
			username,
			req.RemoteAddr,
			res.(*loggingResponseWriter).wroteHeader.
				Sub(res.(*loggingResponseWriter).started).String(),
			res.(*loggingResponseWriter).statusCode,
			http.StatusText(res.(*loggingResponseWriter).statusCode),
			req.URL.Path,
		)
	})
}
